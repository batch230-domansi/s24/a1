const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);
const address = ["258", "Washington Ave NW", "California", "90011"];
const [ houseNumber, street, state, zipCode ] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);
